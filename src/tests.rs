use reqwest::Client;
use url::Url;

use crate::CommafeedApi;

fn get_api() -> CommafeedApi {
    CommafeedApi::new(
        &Url::parse("https://www.commafeed.com").unwrap(),
        "demo",
        "demo",
    )
}

#[tokio::test]
async fn user() {
    let api = get_api();
    let user = api.get_profile(&Client::new()).await.unwrap();
    assert_eq!(user.name, "demo");
}

#[tokio::test]
async fn settings() {
    let api = get_api();
    let settings = api.get_settings(&Client::new()).await.unwrap();
    assert_eq!(settings.language, "en");
}

#[tokio::test]
async fn server_info() {
    let api = get_api();
    let server_info = api.get_server_info(&Client::new()).await.unwrap();
    assert!(server_info.demo_account_enabled);
}

#[tokio::test]
async fn category_tree() {
    let api = get_api();
    let category = api.get_category_tree(&Client::new()).await.unwrap();
    assert_eq!(category.id, "all");
    assert_eq!(category.parent_id, None);
}

#[tokio::test]
async fn create_rename_delete_category() {
    let api = get_api();
    let client = Client::new();
    let id = api.create_category("test123", None, &client).await.unwrap();
    api.modify_category(id, Some("new title"), None, None, &client)
        .await
        .unwrap();
    api.delete_category(id, &client).await.unwrap();
}

#[tokio::test]
async fn get_favicon() {
    let api = get_api();
    let client = Client::new();
    let _data = api.get_favicon(143037065, &client).await.unwrap();
}
