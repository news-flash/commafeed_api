use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SharingSettings {
    pub email: bool,
    pub gmail: bool,
    pub facebook: bool,
    pub twitter: bool,
    pub tumblr: bool,
    pub pocket: bool,
    pub instapaper: bool,
    pub buffer: bool,
}
