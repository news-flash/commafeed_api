use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UnreadCount {
    #[serde(rename = "feedId")]
    feed_id: Option<i64>,
    #[serde(rename = "unreadCount")]
    unread_count: Option<i64>,
    #[serde(rename = "newestItemTime")]
    newest_item_time: Option<i64>,
}
