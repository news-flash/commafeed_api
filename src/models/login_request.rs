use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LoginRequest {
    pub name: String,
    pub password: String,
}
