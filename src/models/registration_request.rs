use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RegistrationRequest {
    pub name: String,
    pub password: String,
    pub email: String,
}
