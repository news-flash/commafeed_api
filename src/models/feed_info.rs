use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FeedInfo {
    pub url: String,
    pub title: String,
}
