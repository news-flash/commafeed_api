use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CollapseRequest {
    pub id: i64,
    pub collapse: bool,
}
