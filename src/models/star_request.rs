use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct StarRequest {
    pub id: String,
    #[serde(rename = "feedId")]
    pub feed_id: i64,
    pub starred: bool,
}
