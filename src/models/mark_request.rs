use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct MarkRequest {
    pub id: String,
    pub read: bool,
    #[serde(rename = "olderThan")]
    pub older_than: Option<i64>,
    pub keywords: Option<String>,
    #[serde(rename = "excludedSubscriptions")]
    pub excluded_subscriptions: Option<Vec<i64>>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct MultipleMarkRequest {
    pub requests: Vec<MarkRequest>,
}
