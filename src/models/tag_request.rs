use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct TagRequest {
    #[serde(rename = "entryId")]
    pub entry_id: i64,
    pub tags: Vec<String>,
}
