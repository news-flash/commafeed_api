use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ProfileModificationRequest {
    #[serde(rename = "currentPassword")]
    pub current_password: String,
    pub email: Option<String>,
    #[serde(rename = "newPassword")]
    pub new_password: Option<String>,
    #[serde(rename = "newApiKey")]
    pub new_api_key: bool,
}
