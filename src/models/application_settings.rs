use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ApplicationSettings {
    #[serde(rename = "publicUrl")]
    pub public_url: String,
    #[serde(rename = "hideFromWebCrawlers")]
    pub hide_from_web_crawlers: bool,
    #[serde(rename = "allowRegistrations")]
    pub allow_registrations: bool,
    #[serde(rename = "strictPasswordPolicy")]
    pub strict_password_policy: bool,
    #[serde(rename = "createDemoAccount")]
    pub create_demo_account: bool,
    #[serde(rename = "googleAnalyticsTrackingCode")]
    pub google_analytics_tracking_code: Option<String>,
    #[serde(rename = "googleAuthKey")]
    pub google_auth_key: Option<String>,
    #[serde(rename = "backgroundThreads")]
    pub background_threads: i32,
    #[serde(rename = "databaseUpdateThreads")]
    pub database_update_threads: i32,
    #[serde(rename = "databaseCleanupBatchSize")]
    pub database_cleanup_batch_size: i32,
    #[serde(rename = "smtpHost")]
    pub smtp_host: Option<String>,
    #[serde(rename = "smtpPort")]
    pub smtp_port: Option<i32>,
    #[serde(rename = "smtpTls")]
    pub smtp_tls: Option<bool>,
    #[serde(rename = "smtpUserName")]
    pub smtp_user_name: Option<String>,
    #[serde(rename = "smtpPassword")]
    pub smtp_password: Option<String>,
    #[serde(rename = "smtpFromAddress")]
    pub smtp_from_address: Option<String>,
    #[serde(rename = "graphiteEnabled")]
    pub graphite_enabled: Option<bool>,
    #[serde(rename = "graphitePrefix")]
    pub graphite_prefix: Option<String>,
    #[serde(rename = "graphitePort")]
    pub graphite_port: Option<i32>,
    #[serde(rename = "graphiteInterval")]
    pub graphite_interval: Option<i32>,
    #[serde(rename = "heavyLoad")]
    pub heavy_load: bool,
    pub pubsubhubbub: bool,
    #[serde(rename = "imageProxyEnabled")]
    pub image_proxy_enabled: bool,
    #[serde(rename = "queryTimeout")]
    pub query_timeout: u32,
    #[serde(rename = "keepStatusDays")]
    pub keep_status_days: u32,
    #[serde(rename = "maxFeedCapacity")]
    pub max_feed_capacity: u32,
    #[serde(rename = "maxFeedsPerUser")]
    pub max_feeds_per_user: i32,
    #[serde(rename = "refreshIntervalMinutes")]
    pub refresh_interval_minutes: u32,
    pub cache: Cache,
    pub announcement: Option<String>,
    #[serde(rename = "userAgent")]
    pub user_agent: Option<String>,
    #[serde(rename = "unreadThreshold")]
    pub unread_threshold: Option<String>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Cache {
    #[serde(rename = "NOOP")]
    Noop,
    #[serde(rename = "REDIS")]
    Redis,
}
