use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SubscribeRequest {
    pub url: String,
    pub title: String,
    #[serde(rename = "categoryId")]
    pub category_id: Option<String>,
}
