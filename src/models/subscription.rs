use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Subscription {
    #[serde(rename = "id")]
    pub subscription_id: i64,
    pub name: String,
    pub message: Option<String>,
    #[serde(rename = "errorCount")]
    pub error_count: i32,
    #[serde(rename = "lastRefresh")]
    pub last_refresh: i64,
    #[serde(rename = "nextRefresh")]
    pub next_refresh: Option<i64>,
    #[serde(rename = "feedUrl")]
    pub feed_url: String,
    #[serde(rename = "feedLink")]
    pub feed_link: String,
    #[serde(rename = "iconUrl")]
    pub icon_url: String,
    pub unread: u64,
    #[serde(rename = "categoryId")]
    pub category_id: Option<String>,
    pub position: Option<i32>,
    #[serde(rename = "newestItemTime")]
    pub newest_item_time: Option<i64>,
    pub filter: Option<String>,
}
