use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AddCategoryRequest {
    pub name: String,
    #[serde(rename = "parentId")]
    pub parent_id: Option<String>,
}
