use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ServerInfo {
    pub announcement: Option<String>,
    pub version: String,
    #[serde(rename = "gitCommit")]
    pub git_commit: String,
    #[serde(rename = "allowRegistrations")]
    pub allow_registrations: bool,
    #[serde(rename = "googleAnalyticsCode")]
    pub google_analytics_code: Option<String>,
    #[serde(rename = "smtpEnabled")]
    pub smtp_enabled: bool,
    #[serde(rename = "demoAccountEnabled")]
    pub demo_account_enabled: bool,
}
