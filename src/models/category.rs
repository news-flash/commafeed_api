use super::subscription::Subscription;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Category {
    pub id: String,
    #[serde(rename = "parentId")]
    pub parent_id: Option<String>,
    #[serde(rename = "parentName")]
    pub parent_name: Option<String>,
    pub name: String,
    pub children: Vec<Category>,
    pub feeds: Vec<Subscription>,
    pub expanded: bool,
    pub position: i32,
}
