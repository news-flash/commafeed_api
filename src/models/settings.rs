use serde::{Deserialize, Serialize};

use super::sharing_settings::SharingSettings;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Settings {
    pub language: String,
    #[serde(rename = "readingMode")]
    pub reading_mode: ReadingMode,
    #[serde(rename = "readingOrder")]
    pub reading_order: ReadingOrder,
    #[serde(rename = "showRead")]
    pub show_read: bool,
    #[serde(rename = "scrollMarks")]
    pub scroll_marks: bool,
    #[serde(rename = "customCss")]
    pub custom_css: Option<String>,
    #[serde(rename = "customJs")]
    pub custom_js: Option<String>,
    #[serde(rename = "scrollSpeed")]
    pub scroll_speed: i32,
    #[serde(rename = "alwaysScrollToEntry")]
    pub always_scroll_to_entry: bool,
    #[serde(rename = "markAllAsReadConfirmation")]
    pub mark_all_as_read_confirmation: bool,
    #[serde(rename = "customContextMenu")]
    pub custom_context_menu: bool,
    #[serde(rename = "sharingSettings")]
    pub sharing_settings: SharingSettings,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub enum ReadingMode {
    #[serde(rename = "all")]
    All,
    #[serde(rename = "unread")]
    Unread,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize, Serialize)]
pub enum ReadingOrder {
    #[serde(rename = "asc")]
    Ascending,
    #[serde(rename = "desc")]
    Descending,
}
