use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FeedModificationRequest {
    pub id: i64,
    pub name: Option<String>,
    #[serde(rename = "categoryId")]
    pub category_id: Option<String>,
    pub position: Option<i32>,
    pub filter: Option<String>,
}
