use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Entry {
    pub id: String,
    pub guid: String,
    pub title: String,
    pub content: Option<String>,
    pub categories: Option<String>,
    pub rtl: bool,
    pub author: Option<String>,
    #[serde(rename = "enclosureUrl")]
    pub enclosure_url: Option<String>,
    #[serde(rename = "enclosureType")]
    pub enclosure_type: Option<String>,
    #[serde(rename = "mediaDescription")]
    pub media_description: Option<String>,
    #[serde(rename = "mediaThumbnailUrl")]
    pub media_thumbnail_url: Option<String>,
    #[serde(rename = "mediaThumbnailWidth")]
    pub media_thumbnail_width: Option<i32>,
    #[serde(rename = "mediaThumbnailHeight")]
    pub media_thumbnail_height: Option<i32>,
    pub date: i64,
    #[serde(rename = "insertedDate")]
    pub inserted_date: i64,
    #[serde(rename = "feedId")]
    pub feed_id: String,
    #[serde(rename = "feedName")]
    pub feed_name: String,
    #[serde(rename = "feedUrl")]
    pub feed_url: String,
    #[serde(rename = "feedLink")]
    pub feed_link: Option<String>,
    #[serde(rename = "iconUrl")]
    pub icon_url: String,
    pub url: String,
    pub read: bool,
    pub starred: bool,
    pub markable: bool,
    pub tags: Vec<String>,
}
