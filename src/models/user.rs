use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct User {
    pub id: i64,
    pub name: String,
    pub email: Option<String>,
    #[serde(rename = "apiKey")]
    pub api_key: Option<String>,
    pub password: Option<String>,
    pub enabled: bool,
    pub created: Option<u64>,
    #[serde(rename = "lastLogin")]
    pub last_login: Option<u64>,
    pub admin: bool,
}
