use serde::{Deserialize, Serialize};

use super::entry::Entry;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Entries {
    pub name: String,
    pub message: Option<String>,
    #[serde(rename = "errorCount")]
    pub error_count: i32,
    #[serde(rename = "feedLink")]
    pub feed_link: Option<String>,
    pub timestamp: i64,
    #[serde(rename = "hasMore")]
    pub has_more: bool,
    pub offset: Option<i32>,
    pub limit: Option<i32>,
    #[serde(rename = "ignoredReadStatus")]
    pub ignored_read_status: bool,
    pub entries: Vec<Entry>,
}
